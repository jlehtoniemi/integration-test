const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
}

const pad2 = (rgb) => {
    return (rgb.length === 0 ? "0"  : rgb);
}

module.exports = {
    rgbToHex: (red, green, blue) =>{
        const redHex = red.toString(16);
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex);// ff0000
    },

    hexToRgb: (red, green, blue) =>{
        const redRgb = parseInt(red, 16);
        const greenRgb = parseInt(green, 16);
        const blueRgb = parseInt(blue, 16);

        return String(pad2(redRgb) + "," + pad2(greenRgb)+ "," + pad2(blueRgb));

    }
}