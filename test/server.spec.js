const expect = require('chai').expect;
const request = require('request');
const app = require('../src/server');
const port = 3000;

describe("Color Code Converter API", () => {
    let server = undefined;  //Toimii ilmankin, mutta loogisempi.
    before("Start server before running tests", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening on localhost:${port}`)
            done();
        })
    })

    // describe("RGB to Hex conversion", () => {
    //     const url = `http://localhost:${port}/rgb-to-hex?red=255&green=255&blue=255`;

    //     it("returns status code 200", (done) => {
    //         request(url, (error, response, body) => {
    //             expect(response.statusCode).to.equal(200);
    //             done();
    //         })
    //     });
    //     it("returns the color in hex", (done) => {
    //         request(url, (error, response, body) => {
    //             expect(body).to.equal("ffffff");
    //             done();
    //         })
    //     })

    // }),

    describe("hex to RGB conversion", () => {
        const url = `http://localhost:${port}/hex-to-rgb?red=ff&green=ff&blue=ff`;

        it("returns status code 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            })
        });
        it("returns the color in hex", (done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal("255,255,255");
                done();
            })
        })

    })


    after("Shut down server after finishing tests", (done) => {
        server.close();
        done();
    })
}) 
